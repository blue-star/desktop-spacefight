class MainScene extends Scene
{
    static Orientation = { horizontal: 0, vertical: 1 };

    tick = 0;
    tieFormationController;
    xwingFormationController;

    constructor (x, y, width, height, displayMode = 0)
    {
        super(x, y, width, height, displayMode);
        this.backgroundColor = { r: 0, b: 0, g: 0 };
    }

    update (deltaTime)
    {
        super.update(deltaTime);

        if (!this.started)
        {
            this.start();
        }

        this.updateOrientation();

        this.tieFormationController.update(deltaTime);
        this.xwingFormationController.update(deltaTime);
    }

    draw ()
    {
        super.draw();
    }

    start ()
    {
        this.addObject(new Background(0, 0, this.game.images.s_background));

        this.tieFormationController = new FormationController(this, FormationController.FlightDirection.left);
        this.xwingFormationController = new FormationController(this, FormationController.FlightDirection.right);

        this.addObject(new Refresh(0, 0, this.game.images.s_refresh, 50, this.tieFormationController, false));
        this.addObject(new Refresh(0, 0, this.game.images.s_refresh, 50, this.xwingFormationController, true));

        this.started = true;
    }

    updateOrientation ()
    {
        if (this.real_size.x > this.real_size.y)
        {
            this.orientation = MainScene.Orientation.horizontal;
        }
        else
        {
            this.orientation = MainScene.Orientation.vertical;
        }
    }
}