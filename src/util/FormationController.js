class FormationController
{
    static FlightDirection = { left: -1, right: 1 };
    static MaxFightersActive = 50;
    static SpawnCooldownSeconds = 2;
    static MaxFormationSize = 5;

    cooldown = 0;

    activeFormations = []; // e.g. [{fighters:[f, f, f]}, {fighters:[f, f, f]}, {fighters:[f, f, f]}]

    static Teams = null;
    team = null;

    get activeFighters ()
    {
        let count = 0;

        for (let i = 0, l = this.activeFormations.length; i < l; i++)
        {
            count += this.activeFormations[i].fighters.length;
        }

        return count;
    }

    constructor (scene, flightDirection)
    {
        this.scene = scene;
        this.flightDirection = flightDirection;
        this.activeFormations = [];

        let images = this.scene.game.images;

        FormationController.Teams = FormationController.Teams ||
            [
                {
                    sameFollowers: true,
                    leadersCanBeFollowers: true,
                    fighters: [// Rebels
                        { sprite: images.s_xwing, blt: images.s_xwingBlt, fireOffset: 12, alternate: true, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_awing, blt: images.s_awingBlt, fireOffset: 9, alternate: true, animSpeed: 0.03, canLead: true, scale: 1 },
                    ]
                },
                {
                    sameFollowers: true,
                    leadersCanBeFollowers: true,
                    fighters: [// Empire
                        { sprite: images.s_tiefighter, blt: images.s_tieBlt, fireOffset: 0, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                    ]
                },
                {
                    sameFollowers: true,
                    leadersCanBeFollowers: true,
                    fighters: [// Star Fox
                        { sprite: images.s_arwing, blt: images.s_arBlt, fireOffset: 0, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                    ]
                },
                {
                    sameFollowers: true,
                    leadersCanBeFollowers: true,
                    fighters: [// Star Wolf
                        { sprite: images.s_wolfen, blt: images.s_wlfBlt, fireOffset: 4, alternate: true, animSpeed: 0.03, canLead: true, scale: 1 },
                    ]
                },
                {
                    sameFollowers: false,
                    leadersCanBeFollowers: false,
                    fighters: [// Galaga
                        { sprite: images.s_galaga1, blt: images.s_galagaBlt1, fireOffset: 0, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_galaga2, blt: images.s_galagaBlt1, fireOffset: 0, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_galaga3, blt: images.s_galagaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 1 },
                        { sprite: images.s_galaga4, blt: images.s_galagaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 1 },
                        { sprite: images.s_galaga5, blt: images.s_galagaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 1 },
                        { sprite: images.s_galaga6, blt: images.s_galagaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 1 },
                    ]
                },
                {
                    sameFollowers: false,
                    leadersCanBeFollowers: false,
                    fighters: [// Amiga Galaga Deluxe
                        { sprite: images.s_amiga1, blt: images.s_amigaBlt1, fireOffset: 0, alternate: false, animSpeed: 0.03, canLead: true, scale: 0.5 },
                        { sprite: images.s_amiga5, blt: images.s_amigaBlt1, fireOffset: 0, alternate: false, animSpeed: 0.03, canLead: true, scale: 0.5 },
                        { sprite: images.s_amiga2, blt: images.s_amigaBlt3, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 0.5 },
                        { sprite: images.s_amiga3, blt: images.s_amigaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 0.5 },
                        { sprite: images.s_amiga4, blt: images.s_amigaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 0.5 },
                        { sprite: images.s_amiga6, blt: images.s_amigaBlt3, fireOffset: 0, alternate: false, animSpeed: 0.5, canLead: false, scale: 0.5 },
                        { sprite: images.s_amiga7, blt: images.s_amigaBlt2, fireOffset: 0, alternate: false, animSpeed: 0.3, canLead: false, scale: 0.5 },
                    ]
                },
                {
                    sameFollowers: false,
                    leadersCanBeFollowers: true,
                    fighters: [// Gundam
                        { sprite: images.s_gundam1, blt: images.s_gundamBlt, fireOffset: 7, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_gundam2, blt: images.s_gundamBlt, fireOffset: 7, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_gundam3, blt: images.s_gundamBlt, fireOffset: 7, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_gundam4, blt: images.s_gundamBlt, fireOffset: 7, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_gundam5, blt: images.s_gundamBlt, fireOffset: 7, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                        { sprite: images.s_gundam6, blt: images.s_gundamBlt, fireOffset: 7, alternate: false, animSpeed: 0.03, canLead: true, scale: 1 },
                    ]
                },
                {
                    sameFollowers: true,
                    leadersCanBeFollowers: false,
                    fighters: [// Tim
                        { sprite: images.s_tim1, blt: images.s_timBlt1, fireOffset: 6, alternate: true, animSpeed: 0.03, canLead: true, scale: 0.7 },
                        { sprite: images.s_tim2, blt: images.s_timBlt2, fireOffset: 2, alternate: true, animSpeed: 0.03, canLead: false, scale: 0.7 },
                    ]
                },
            ];

        FormationController.availableTeams = FormationController.Teams.slice();

        this.pickRandomTeam();
    }

    pickRandomTeam ()
    {
        console.log("Picking a new team");
        this.team = FormationController.availableTeams.splice(Math.floor(Math.random() * FormationController.availableTeams.length), 1)[0];

        if (FormationController.availableTeams.length == 0)
        {
            FormationController.availableTeams = FormationController.Teams.slice();
        }
    }

    update (deltaTime)
    {
        for (let i = 0, l = this.activeFormations.length; i < l; i++)
        {
            this.activeFormations[i].fighters = this.activeFormations[i].fighters.filter(o => !o.destroyed);

            //whichever fighter is index 0 in the formation is the formation leader
            for (let f = 0, ft = this.activeFormations[i].fighters.length; f < ft; f++)
            {
                if (f == 0)
                {
                    this.activeFormations[i].fighters[f].targetFlightSpeed = this.activeFormations[i].fighters[f].maxFlightSpeed * 0.8;

                    if (this.scene.orientation == MainScene.Orientation.horizontal)
                    {
                        this.activeFormations[i].fighters[f].flightTargetVector.x = this.flightDirection * 100;
                        this.activeFormations[i].fighters[f].flightTargetVector.y = 100 + ((this.scene.real_size.y - 200) * this.activeFormations[i].yOffset - this.activeFormations[i].fighters[f].transform.position.y);
                    }
                    else
                    {
                        this.activeFormations[i].fighters[f].flightTargetVector.y = this.flightDirection * 100;
                        this.activeFormations[i].fighters[f].flightTargetVector.x = this.scene.real_size.x * this.activeFormations[i].yOffset - this.activeFormations[i].fighters[f].transform.position.x;
                    }
                }
                else
                {
                    let offset = this.activeFormations[i].fighters[0].transform.position.getCopy();
                    offset = offset.add(this.activeFormations[i].fighters[0].transform.right.stretch((((f + 0.5) % 2) - 1) * 65 * Math.ceil(f / 2)));
                    offset = offset.add(this.activeFormations[i].fighters[0].transform.back.stretch(35 * (Math.ceil(f / 2) - 1.5)));

                    this.activeFormations[i].fighters[f].flightTargetVector = offset.subtract(this.activeFormations[i].fighters[f].transform.position);
                    this.activeFormations[i].fighters[f].targetFlightSpeed = this.activeFormations[i].fighters[f].flightTargetVector.length * 2;
                }
            }
        }

        this.activeFormations = this.activeFormations.filter(a => a.fighters.length > 0);

        if (this.activeFighters <= FormationController.MaxFightersActive - FormationController.MaxFormationSize)
        {
            if (this.cooldown <= 0)
            {
                this.cooldown = FormationController.SpawnCooldownSeconds;

                let spawnOffset = Math.random();
                this.activeFormations.push({ yOffset: spawnOffset, fighters: [] });

                let leaders = this.team.fighters.filter(f => f.canLead);
                let leader = leaders[Math.floor(Math.random() * leaders.length)];
                let follower = leader;
                let followers = null;

                if (this.team.fighters.some(f => !f.canLead))
                {
                    if (this.team.leadersCanBeFollowers)
                    {
                        followers = this.team.fighters;
                    }
                    else
                    {
                        followers = this.team.fighters.filter(f => !f.canLead);
                    }

                    follower = followers[Math.floor(Math.random() * followers.length)];
                }

                for (let i = 0; i < FormationController.MaxFormationSize; i++)
                {
                    if (!this.team.sameFollowers)
                    {
                        if (this.team.fighters.some(f => !f.canLead))
                        {
                            follower = followers[Math.floor(Math.random() * followers.length)];
                        }
                        else
                        {
                            follower = leaders[Math.floor(Math.random() * leaders.length)];
                        }
                    }

                    let fighter = i == 0 ? leader : follower;
                    let newFighter;

                    if (this.scene.orientation == MainScene.Orientation.horizontal) 
                    {
                        let spawnY = this.scene.real_size.y * spawnOffset;
                        let spawnX = this.flightDirection == FormationController.FlightDirection.right ? -50 : this.scene.real_size.x + 50;
                        newFighter = new GenFighter(spawnX, spawnY, fighter.sprite, fighter.blt, fighter.fireOffset, fighter.alternate, this.flightDirection, fighter.animSpeed);
                    }
                    else 
                    {
                        let spawnY = this.flightDirection == FormationController.FlightDirection.right ? -50 : this.scene.real_size.y + 50;
                        let spawnX = this.scene.real_size.x * spawnOffset;
                        newFighter = new GenFighter(spawnX, spawnY, fighter.sprite, fighter.blt, fighter.fireOffset, fighter.alternate, this.flightDirection, fighter.animSpeed);
                    }

                    newFighter.transform.scale.x = fighter.scale;
                    newFighter.transform.scale.y = fighter.scale;

                    this.activeFormations[this.activeFormations.length - 1].fighters.push(newFighter);
                    this.scene.addObject(newFighter);
                }
            }
            else
            {
                this.cooldown -= deltaTime;
            }
        }
    }


}