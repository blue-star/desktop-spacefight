class Refresh extends GameObject
{

    constructor (x, y, sprite, layer, formationController, left) 
    {
        super(x, y, sprite, layer);
        this.formationController = formationController;
        this.left = left;
    }

    update ()
    {
        this.transform.position.x = this.left ? 15 : this.scene.real_size.x - 15;
        this.transform.position.y = 15;

        if (GameInput && GameInput.mousePosition && this.transform.position.distanceTo(GameInput.mousePosition) < 30)
        {
            if (GameInput.mousePressed(GameInput.MouseButtons.Left))
            {
                this.formationController.pickRandomTeam();
            }
        }
    }

    draw ()
    {
        if (GameInput && GameInput.mousePosition && this.transform.position.distanceTo(GameInput.mousePosition) < 30)
        {
            super.draw();
        }
    }
}