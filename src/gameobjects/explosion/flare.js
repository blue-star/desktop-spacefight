class Flare extends GameObject
{
    constructor (x, y, sprite, velocity, size) 
    {
        super(x, y, sprite, -1);

        this.velocity = velocity.stretch(Math.randomRange(0.6, 1.2));
        this.transform.rotation = Math.random() * 360;
        this.transform.scale.x = size;
        this.transform.scale.y = size;
    }

    update (deltaTime)
    {
        super.update(deltaTime);

        if (this.renderer.subImage >= this.renderer.sprite.subImages - 1)
        {
            this.destroy();
        }

        if (this.renderer.subImage == 3 && this.transform.scale.x - 0.03 > 0)
        {
            this.scene.addObject(new Flare(this.transform.position.x + this.velocity.x, this.transform.position.y + this.velocity.y, this.renderer.sprite, this.velocity, this.transform.scale.x - 0.03));
        }

        this.renderer.subImage++;
    }
}