class Explosion extends GameObject
{
    constructor (x, y, sprite, velocity) 
    {
        super(x, y, sprite, -1);

        this.transform.velocity = velocity;
        this.transform.rotation = Math.random() * 360;

        this.flared = false;
    }

    update (deltaTime)
    {
        super.update(deltaTime);

        if (!this.flared)
        {
            this.flared = true;

            for (let i = 0, c = 2 + (Math.random() * 4); i < c; i++)
            {
                let velocity = this.transform.velocity.getCopy();
                velocity.x += (-0.5 + Math.random()) * 150;
                velocity.y += (-0.5 + Math.random()) * 150;
                this.scene.addObject(new Flare(this.transform.position.x, this.transform.position.y, this.scene.game.images.s_flare, velocity.stretch(0.1), 0.3));
            }
        }

        if (this.renderer.subImage >= this.renderer.sprite.subImages - 1)
        {
            this.destroy();
        }

        this.renderer.subImage++;
    }
}