class GenFighter extends Fighter
{
    constructor (x, y, sprite, bulletSprite, fireOffset, alternate, team, animSpeed = 0.03) 
    {
        super(x, y, sprite, 0, team, animSpeed);

        this.bulletSprite = bulletSprite;
        this.collider.enabled = false;
        this.fireOffset = fireOffset;
        this.alternate = alternate;
    }

    fire ()
    {
        let offset = this.transform.position.add(this.transform.right.stretch(this.fireOffset));
        let x = offset.x;
        let y = offset.y;

        if (this.alternate)
            this.fireOffset *= -1;

        let blt = new Bullet(x, y, this.bulletSprite, this.transform.forward.stretch(this.bulletSpeed), this.team);
        this.scene.addObject(blt);
    }

    getClass ()
    {
        return GenFighter;
    }
}