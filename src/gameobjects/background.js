class Background extends GameObject
{
    constructor (x, y, sprite) 
    {
        super(x, y, sprite, -1);
        let params = new URLSearchParams(window.location.search);
        let passthrough = decodeURIComponent(params.get("passthrough"));

        if (passthrough)
        {
            this.passthroughImage = new Image();
            this.passthroughImage.src = passthrough;
            this.passthroughImage.setAttribute("style", "display:none");
            var background = this;
            this.passthroughImage.onload = function ()
            {
                background.renderer.sprite = new image(background.passthroughImage, 1, 1);
            };
        }
    }

    update (deltaTime)
    {
        this.transform.position.x = this.scene.real_size.x / 2;
        this.transform.position.y = this.scene.real_size.y / 2;

        let scale = Math.max(this.scene.real_size.x / this.renderer.sprite.width, this.scene.real_size.y / this.renderer.sprite.height);

        this.transform.scale = new vector(scale, scale);
    }
}