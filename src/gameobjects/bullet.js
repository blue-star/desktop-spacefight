class Bullet extends GameObject
{
    constructor (x, y, sprite, velocity, team) 
    {
        super(x, y, sprite, -1);

        this.transform.velocity = velocity;
        this.transform.rotation = velocity.toAngle();

        this.team = team;

        if (this.renderer.sprite.hasAnimationCycle("fly"))
        {
            this.renderer.setAnimation("fly", 0, 0.05);
        }
    }

    update (deltaTime)
    {
        super.update(deltaTime);

        this.postUpdate();
    }

    postUpdate ()
    {
        if (this.transform.position.x > this.scene.real_size.x)
        {
            this.destroy();
        }

        if (this.transform.position.y > this.scene.real_size.y)
        {
            this.destroy();
        }

        if (this.transform.position.x < 0)
        {
            this.destroy();
        }

        if (this.transform.position.y < 0)
        {
            this.destroy();
        }
    }
}