class Fighter extends GameObject
{
    static Teams = { Right: 1, Left: -1 };

    get fireCooldown ()
    {
        if (!this.scene)
            return 1;

        let close = 0.1;
        let far = 0.9;

        let p = this.transform.position.x / this.scene.real_size.x;
        if (this.team == Fighter.Teams.Right)
            p = 1 - p;

        return Math.max(Math.lerp(far, close, Math.pow(p, 2)), close);
    }

    constructor (x, y, sprite, layer, team, animSpeed = 0.03) 
    {
        super(x, y, sprite, layer);

        this.flightTargetVector = new vector(0, 0);
        this.collider.enabled = false;
        this.renderer.setAnimation("fly", 0, animSpeed);

        this.renderer.cycle.timer += Math.random();

        this.targetFlightSpeed = 100;
        this.minFlightSpeed = 100;
        this.maxFlightSpeed = 150;
        this.team = team;

        this.tags.push(this.team == Fighter.Teams.Right ? "Right" : "Left");
        this.enemyTeam = this.team != Fighter.Teams.Right ? "Right" : "Left";

        this.cooldown = Math.random() * this.fireCooldown;
        this.retreatRange = 150;
        this.unRetreatRange = 300;

        this.engageRange = 400;
        this.evadeRangeMax = 500;
        this.evadeRangeMin = 100;
        this.fireRange = 300;
        this.shouldFire = false;
        this.retreating = false;
        this.target = null;
        this.bulletSpeed = 500;

        this.collisionRange = 15;

        this.screenBoundsSafeRange = 50;

        this.turnWeight = 3;
    }

    findTarget ()
    {
        if (this.target == null || this.target.destroyed)
        {
            let opposingFightersInRange = this.quadTree.getObjectsWithTagInRange(this.transform.position.add(this.transform.forward.stretch(this.engageRange * 0.33)), this.engageRange, [this.enemyTeam]);

            this.retreating = false;

            if (opposingFightersInRange.length > 0)
                this.target = opposingFightersInRange[0];
        }
    }

    tryShoot ()
    {
        if (this.cooldown < 0)
        {
            this.cooldown = this.fireCooldown;

            this.fire();
        }
    }

    tryDogfight (deltaTime)
    {
        if (this.target == null || this.target.destroyed)
        {
            this.target = null;
            return false;
        }

        let distanceToTarget = this.transform.position.distanceTo(this.target.transform.position);

        if (this.retreating)
        {
            let aimTarget = this.calculateAimVector();

            this.flyTowards(this.transform.position.add(this.transform.forward.stretch(50)).add(aimTarget.stretch(-100)), deltaTime);

            if (distanceToTarget > this.unRetreatRange)
                this.retreating = false;
        }
        else
        {
            let aimTarget = this.calculateAimVector();
            this.flyTowards(this.transform.position.add(aimTarget), deltaTime);

            if (this.transform.forward.distanceTo(aimTarget) < 0.15)
                this.tryShoot();

            if (distanceToTarget < this.retreatRange)
                this.retreating = true;
        }

        return true;
    }

    calculateAimVector ()
    {
        let distance = this.target.transform.position.distanceTo(this.transform.position);

        let timeToTarget = distance / this.bulletSpeed;
        let futureX = this.target.transform.position.x + this.target.transform.velocity.x * timeToTarget;
        let futureY = this.target.transform.position.y + this.target.transform.velocity.y * timeToTarget;

        let aimVector = new vector(
            futureX - this.transform.position.x,
            futureY - this.transform.position.y
        );

        return aimVector.normalize();
    }

    update (deltaTime)
    {
        this.targetVelocity = this.transform.velocity;

        super.update(deltaTime);

        this.findTarget();
        let flightTarget = null;

        let canDodge = false;
        if (!this.tryDogfight(deltaTime))
        {
            let p = this.transform.position.x / this.scene.real_size.x;
            if (this.team == Fighter.Teams.Right)
                p = 1 - p;

            canDodge = p > 0.25;
            flightTarget = this.transform.position.add(this.flightTargetVector);
        }

        if (!this.checkCollision(canDodge, deltaTime) && flightTarget != null)
        {
            this.flyTowards(flightTarget, deltaTime);
        }

        this.cooldown -= deltaTime;

        if (this.targetVelocity.length < this.minFlightSpeed)
            this.targetVelocity = this.targetVelocity.normalize().stretch(this.minFlightSpeed);

        if (this.targetVelocity.length > this.maxFlightSpeed)
            this.targetVelocity = this.targetVelocity.normalize().stretch(this.maxFlightSpeed);

        this.transform.velocity = this.targetVelocity;

        this.postUpdate();
    }

    flyTowards (target, deltaTime)
    {
        target.x = Math.clamp(target.x, this.screenBoundsSafeRange, this.scene.real_size.x - this.screenBoundsSafeRange);
        target.y = Math.clamp(target.y, this.screenBoundsSafeRange, this.scene.real_size.y - this.screenBoundsSafeRange);

        this.targetVelocity = this.targetVelocity.lerpTo(this.generateFlightVector(target.subtract(this.transform.position)), this.turnWeight * deltaTime);

        this.transform.rotation = this.transform.velocity.toAngle();
    }

    generateFlightVector (vector)
    {
        return vector.normalize().stretch(Math.clamp(this.targetFlightSpeed, this.minFlightSpeed, this.maxFlightSpeed));
    }


    postUpdate ()
    {
        if (this.transform.position.x > this.scene.real_size.x - 5 && this.team == Fighter.Teams.Right)
        {
            this.destroy();
        }

        if (this.transform.position.y > this.scene.real_size.y)
        {
            this.destroy();
        }

        if (this.transform.position.x < 5 && this.team == Fighter.Teams.Left)
        {
            this.destroy();
        }

        if (this.transform.position.y < 0)
        {
            this.destroy();
        }
    }

    checkCollision (canDodge, deltaTime)
    { //returns true is the fighter should attempt to evade incoming fire
        let bulletsInRange = this.quadTree.getObjectsOfTypesInRange(this.transform.position, canDodge ? this.evadeRangeMax : this.collisionRange, [Bullet])
            .filter(o => this.team != o.team && !o.destroyed);

        let closestBullet = bulletsInRange[0];

        if (bulletsInRange.length > 0 && closestBullet.transform.position.distanceTo(this.transform.position) < this.collisionRange)
        {
            this.destroy();
            closestBullet.destroy();
            return false;
        }

        if (canDodge)
        {
            let fwd = this.transform.position.add(this.transform.forward);
            let bck = this.transform.position.add(this.transform.back);
            let bulletDelta = null;

            closestBullet = bulletsInRange.find(
                b =>
                {
                    bulletDelta = this.transform.position.subtract(b.transform.position).normalize();
                    let isInFront = b.transform.position.distanceTo(fwd) < b.transform.position.distanceTo(bck);
                    let isAimedAtMe = bulletDelta.distanceTo(b.transform.forward) < 0.2;
                    return isInFront && isAimedAtMe && b.transform.position.distanceTo(this.transform.position) > this.evadeRangeMin;
                }
            );

            if (closestBullet)
            { // dodging bullets
                if (bulletDelta.distanceTo(closestBullet.transform.left) < bulletDelta.distanceTo(closestBullet.transform.right))
                {
                    //steer to bullet's left
                    this.flyTowards(closestBullet.transform.position.add(closestBullet.transform.left.stretch(3500)), deltaTime);
                }
                else
                {
                    //steer to bullet's right
                    this.flyTowards(closestBullet.transform.position.add(closestBullet.transform.right.stretch(3500)), deltaTime);
                }

                return true;
            }
        }

        return false;
    }

    onClick ()
    {
        this.destroy();
    }

    onDestroy ()
    {
        this.scene.addObject(new Explosion(this.transform.position.x, this.transform.position.y, this.scene.game.images.s_explosion, this.transform.velocity));
    }
}