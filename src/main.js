console.log("Game started!");
var game = new Game();

window.onload = function ()
{
    game.preloadImagesThenStart(
        [
            { name: "s_refresh", url: "images/refresh.png", subImgTotal: 1, perRow: 1 },
            { name: "s_tiefighter", url: "images/tiefighter.png", subImgTotal: 3, perRow: 3 },
            { name: "s_xwing", url: "images/xwing.png", subImgTotal: 3, perRow: 3 },
            { name: "s_awing", url: "images/awing.png", subImgTotal: 3, perRow: 3 },
            { name: "s_arwing", url: "images/arwing.png", subImgTotal: 3, perRow: 3 },
            { name: "s_wolfen", url: "images/wolfen.png", subImgTotal: 3, perRow: 3 },

            { name: "s_tim1", url: "images/tim1.png", subImgTotal: 3, perRow: 3 },
            { name: "s_tim2", url: "images/tim2.png", subImgTotal: 3, perRow: 3 },

            { name: "s_galaga1", url: "images/galaga1.png", subImgTotal: 3, perRow: 3 },
            { name: "s_galaga2", url: "images/galaga2.png", subImgTotal: 3, perRow: 3 },
            { name: "s_galaga3", url: "images/galaga3.png", subImgTotal: 2, perRow: 2 },
            { name: "s_galaga4", url: "images/galaga4.png", subImgTotal: 2, perRow: 2 },
            { name: "s_galaga5", url: "images/galaga5.png", subImgTotal: 2, perRow: 2 },
            { name: "s_galaga6", url: "images/galaga6.png", subImgTotal: 2, perRow: 2 },

            { name: "s_amiga1", url: "images/amiga1.png", subImgTotal: 3, perRow: 3 },
            { name: "s_amiga2", url: "images/amiga2.png", subImgTotal: 2, perRow: 2 },
            { name: "s_amiga3", url: "images/amiga3.png", subImgTotal: 2, perRow: 2 },
            { name: "s_amiga4", url: "images/amiga4.png", subImgTotal: 2, perRow: 2 },
            { name: "s_amiga5", url: "images/amiga5.png", subImgTotal: 3, perRow: 3 },
            { name: "s_amiga6", url: "images/amiga6.png", subImgTotal: 2, perRow: 2 },
            { name: "s_amiga7", url: "images/amiga7.png", subImgTotal: 3, perRow: 3 },

            { name: "s_gundam1", url: "images/gundam1.png", subImgTotal: 3, perRow: 3 },
            { name: "s_gundam2", url: "images/gundam2.png", subImgTotal: 3, perRow: 3 },
            { name: "s_gundam3", url: "images/gundam3.png", subImgTotal: 3, perRow: 3 },
            { name: "s_gundam4", url: "images/gundam4.png", subImgTotal: 3, perRow: 3 },
            { name: "s_gundam5", url: "images/gundam5.png", subImgTotal: 3, perRow: 3 },
            { name: "s_gundam6", url: "images/gundam6.png", subImgTotal: 3, perRow: 3 },

            { name: "s_xwingBlt", url: "images/xwingblt.png", subImgTotal: 1, perRow: 1 },
            { name: "s_awingBlt", url: "images/awingblt.png", subImgTotal: 1, perRow: 1 },
            { name: "s_tieBlt", url: "images/tieblt.png", subImgTotal: 1, perRow: 1 },
            { name: "s_arBlt", url: "images/arblt.png", subImgTotal: 1, perRow: 1 },
            { name: "s_wlfBlt", url: "images/wlfblt.png", subImgTotal: 1, perRow: 1 },
            { name: "s_timBlt1", url: "images/timBlt1.png", subImgTotal: 6, perRow: 6 },
            { name: "s_timBlt2", url: "images/timBlt2.png", subImgTotal: 6, perRow: 6 },
            { name: "s_galagaBlt1", url: "images/galagaBlt1.png", subImgTotal: 1, perRow: 1 },
            { name: "s_galagaBlt2", url: "images/galagaBlt2.png", subImgTotal: 1, perRow: 1 },
            { name: "s_amigaBlt1", url: "images/amigaBlt1.png", subImgTotal: 2, perRow: 2 },
            { name: "s_amigaBlt2", url: "images/amigaBlt2.png", subImgTotal: 1, perRow: 1 },
            { name: "s_amigaBlt3", url: "images/amigaBlt3.png", subImgTotal: 2, perRow: 2 },
            { name: "s_gundamBlt", url: "images/gundamBlt.png", subImgTotal: 2, perRow: 2 },

            { name: "s_background", url: "images/background.png", subImgTotal: 1, perRow: 1 },
            { name: "s_explosion", url: "images/explosion.png", subImgTotal: 16, perRow: 4 },
            { name: "s_flare", url: "images/flare.png", subImgTotal: 25, perRow: 5 }
        ], function ()
    {
        game.images.s_awing.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_xwing.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_tiefighter.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_arwing.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_wolfen.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_tim1.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_tim2.addAnimationCycle("fly", [0, 1, 0, 2], true);

        game.images.s_galaga1.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_galaga2.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_galaga3.addAnimationCycle("fly", [0, 1], true);
        game.images.s_galaga4.addAnimationCycle("fly", [0, 1], true);
        game.images.s_galaga5.addAnimationCycle("fly", [0, 1], true);
        game.images.s_galaga6.addAnimationCycle("fly", [0, 1], true);

        game.images.s_amiga1.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_amiga2.addAnimationCycle("fly", [0, 1], true);
        game.images.s_amiga3.addAnimationCycle("fly", [0, 1], true);
        game.images.s_amiga4.addAnimationCycle("fly", [0, 1], true);
        game.images.s_amiga5.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_amiga6.addAnimationCycle("fly", [0, 1], true);
        game.images.s_amiga7.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_amigaBlt1.addAnimationCycle("fly", [0, 1], true);
        game.images.s_amigaBlt3.addAnimationCycle("fly", [0, 1], true);

        game.images.s_gundam1.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_gundam2.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_gundam3.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_gundam4.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_gundam5.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_gundam6.addAnimationCycle("fly", [0, 1, 0, 2], true);
        game.images.s_timBlt1.addAnimationCycle("fly", [0, 1, 2, 3, 4, 5], true);
        game.images.s_timBlt2.addAnimationCycle("fly", [0, 1, 2, 3, 4, 5], true);
        game.images.s_gundamBlt.addAnimationCycle("fly", [0, 1], true);

        this.createMainScene(game);
    });
};

createMainScene = function (game)
{
    var scene = new MainScene(0, 0, 1, 1, Scene.DisplayModes.relativeToParent);

    game.loadScene(scene);
};
